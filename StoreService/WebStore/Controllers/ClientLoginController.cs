﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStore.Common;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class ClientLoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(ClientLoginModel model)
        {
            if (ModelState.IsValid)
            {
                localhost.Service service = new localhost.Service();

                var result = service.LoginCustomer(model.UserName, model.Password);
                if (result == 1)
                {
                    var user = service.getCustomerByName(model.UserName);
                    var userSession = new UserLogin();
                    userSession.UserName = user.Username;
                    userSession.UserID = user.Id;

                    Session.Add(CommonConstants.Client_SESSION, userSession);
                    Session.Add(CommonConstants.ClientName, user.Username);
                    return RedirectToAction("Index", "Home");
                }
                else if (result == 0)
                {

                    ModelState.AddModelError("", "Account does not exist");
                }
                else if (result == -1)
                {

                    ModelState.AddModelError("", "Wrong password");
                }
                else
                {

                    ModelState.AddModelError("", "wrong input");
                }

            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            var admin = new UserLogin();
            if (this.Session[CommonConstants.Client_SESSION] != null)
            {
                admin = (UserLogin)this.Session[CommonConstants.Client_SESSION];
            }
            if (string.IsNullOrEmpty(admin.UserID))
            {
                RedirectToAction("Login", "ClientLogin");
            }

            this.Session[CommonConstants.Client_SESSION] = null;
            this.Session[CommonConstants.ClientName] = null;
            return RedirectToAction("Index");
        }
	}
}
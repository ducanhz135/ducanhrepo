﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebStore.Common;
using WebStore.localhost;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class CartController : BaseController
    {
        private const string CartSession = "CartSession";
        //
        // GET: /Cart/
        public ActionResult Index()
        {
            var cartItem = Session[CartSession];
            var list = new List<CartItem>();
            if (cartItem != null)
            {
                list = (List<CartItem>)cartItem;
            }
            return View(list);
        }
        public JsonResult DeleteAll()
        {
            Session[CartSession] = null;
            return Json(new
            {
                status = true
            });
        }

        public JsonResult Delete(String id)
        {
            var sessionCart = (List<CartItem>)Session[CartSession];
            sessionCart.RemoveAll(x => x.product.Id == id);
            Session[CartSession] = sessionCart;

            return Json(new
            {
                status = true
            });
        }
        public JsonResult Update(string cartModel)
        {
            var jsonCart = new JavaScriptSerializer().Deserialize<List<CartItem>>(cartModel);
            var sessionCart = (List<CartItem>)Session[CartSession];
            foreach (var item in sessionCart)
            {
                var jsonItem = jsonCart.SingleOrDefault(x => x.product.Id == item.product.Id);
                if (jsonCart != null)
                {
                    item.Quantity = jsonItem.Quantity;
                }
            }
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }

        public ActionResult AddItem(String productId, int quantity)
        {
            localhost.Service service = new localhost.Service();
            var product = service.GetProductById(productId);
            var cart = Session[CartSession];
            if (cart != null)
            {
                var list = (List<CartItem>)cart;
                if (list.Exists(x => x.product.Id.Equals(product.Id)))
                {
                    foreach (var item in list)
                    {
                        if (item.product.Id.Equals(product.Id))
                        {
                            item.Quantity += quantity;
                        }
                    }
                }
                else
                {
                    // create new cart item
                    var item = new CartItem();
                    item.product = product;
                    item.Quantity = quantity;
                    list.Add(item);
                }
                // assign to session
                Session[CartSession] = list;

            }
            else
            {
                // create new cart item
                var item = new CartItem();
                item.product = product;
                item.Quantity = quantity;
                var list = new List<CartItem>();
                list.Add(item);
                // assign to session
                Session[CartSession] = list;
            }
            return RedirectToAction("Index");
        }
        public void GetCustomer()
        {
            localhost.Service service = new localhost.Service();
            List<Customer> cusList = service.GetCustomers().ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            for (int i = 0; i < cusList.Count; i++)
            {
                li.Add(new SelectListItem { Text = cusList.ElementAt(i).Username, Value = cusList.ElementAt(i).Id });
            }
            ViewData["customers"] = li;
            System.Diagnostics.Debug.WriteLine("Number of customers: " + li.Count);
        }

        public ActionResult Payment()
        {
            
            var cartItem = Session[CartSession];
            var list = new List<CartItem>();
            if (cartItem != null)
            {
                list = (List<CartItem>)cartItem;
            }
            var client = (UserLogin)Session[Common.CommonConstants.Client_SESSION];
            ViewBag.clientID = client.UserID;
            return View(list);
        }

        [HttpPost]
        public ActionResult Payment(string Id, string Name, string address, string CusId)
        {
            localhost.Service service = new localhost.Service();
            var order = new WebStore.localhost.TheOrder();
            order.Id = Id;
            order.ShipAddress = address;
            order.Name = Name;
            order.CusId = CusId;
            

            try
            {

                var id = service.InsertOrder(order);
                var cartItem = (List<CartItem>)Session[CartSession];
                
                
                foreach (var item in cartItem)
                {
                    var orderDetail = new WebStore.localhost.OrderDetail();
                    orderDetail.Id = id + item.product.Id;
                    orderDetail.OrdId = id;
                    orderDetail.ProdId = item.product.Id;
                    orderDetail.Price = item.product.Price;
                    orderDetail.Quantity = item.Quantity;
                    var currentid = service.InsertOrderDetail(orderDetail);
                    
                }

            }
            catch (Exception ex)
            {
                string a = ex.Message;

                
            }

            return Redirect("Success");
        }

        public ActionResult Success()
        {
            return View();
        }
	}
}
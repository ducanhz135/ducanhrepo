﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebStore.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            localhost.Service service = new localhost.Service();
            var model = service.GetProducts().ToList();
            ViewBag.Model = model;
            
            return View();
        }
        public JsonResult ListName(string q)
        {
            localhost.Service service = new localhost.Service();
            var data = service.ListName(q);
            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string keyword)
        {
            localhost.Service service = new localhost.Service();
            var model = service.searchProduct(keyword).ToList();
            ViewBag.Model = model;
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
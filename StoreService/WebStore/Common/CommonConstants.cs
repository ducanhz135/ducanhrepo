﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebStore.Common
{
    public static class CommonConstants
    {
        public static string USER_SESSION = "USER_SESSION";
        public static string CartSession = "CartSession";
        public static string USERName = "USERName";
        public static string Client_SESSION = "Client_SESSION";
        public static string ClientName = "ClientName";
    }
}
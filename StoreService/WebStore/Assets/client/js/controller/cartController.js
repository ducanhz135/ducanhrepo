﻿var cart = {
    init: function () {
        cart.RegEvents();
    },
    RegEvents: function () {
        $('#btnContinue').off('click').on('click', function () {
            window.location.href = "/";
        });

        $('#btnPayment').off('click').on('click', function () {
            window.location.href = "/Cart/Payment";
        });

        $('#btnUpdate').off('click').on('click', function () {
            var listProduct = $('.txtQuantity');
            var cartList = [];
            $.each(listProduct, function (i, item) {
                cartList.push({
                    product: {
                        ID: $(item).data('id')
                    },
                    Quantity: $(item).val()
                });
            });

            $.ajax({
                url: '/Cart/Update',
                data: { cartModel: JSON.stringify(cartList) },
                dataType: 'json',
                type: 'POST',
                success: function (e) {
                    if (e.status == true) {
                        window.location.href = "/Cart/Index";
                    }
                }
            });

        });


        $('#btnDeleteAll').off('click').on('click', function () {

            $.ajax({
                url: '/Cart/DeleteAll',
                dataType: 'json',
                type: 'POST',
                success: function (e) {
                    if (e.status == true) {
                        window.location.href = "/Cart/Index";
                    }
                }
            });
        });

        $('.btn-delete').off('click').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                data: { id: $(this).data('id') },
                url: '/Cart/Delete',
                dataType: 'json',
                type: 'POST',
                success: function (e) {
                    if (e.status == true) {
                        window.location.href = "/Cart/Index";
                    }
                }
            });
        });
    }
}
cart.init();
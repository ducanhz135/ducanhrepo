﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStore.Areas.Admin.Models;
using WebStore.Common;

namespace WebStore.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Admin/Login/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                localhost.Service service = new localhost.Service();

                var result = service.LoginAdmin(model.UserName, model.Password);
                if (result == 1)
                {
                    var user = service.getAdminByName(model.UserName);
                    var userSession = new UserLogin();
                    userSession.UserName = user.username;
                    userSession.UserID = user.stfId;

                    Session.Add(CommonConstants.USER_SESSION, userSession);
                    Session.Add(CommonConstants.USERName, user.username);
                    return RedirectToAction("Index", "Home");
                }
                else if (result == 0)
                {
                    
                    ModelState.AddModelError("", "Account does not exist");
                }
                else if (result == -1)
                {
                    
                    ModelState.AddModelError("", "Wrong password");
                }
                else
                {
                    
                    ModelState.AddModelError("", "wrong input");
                }

            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            var admin = new UserLogin();
            if (this.Session[CommonConstants.USER_SESSION] != null)
            {
                admin = (UserLogin)this.Session[CommonConstants.USER_SESSION];
            }
            if (string.IsNullOrEmpty(admin.UserID))
            {
                RedirectToAction("Login", "AdminHome");
            }

            this.Session[CommonConstants.USER_SESSION] = null;
            this.Session[CommonConstants.USERName] = null;
            return RedirectToAction("Index");
        }
	}
}
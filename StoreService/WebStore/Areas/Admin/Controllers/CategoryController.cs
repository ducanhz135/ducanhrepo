﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebStore.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        //
        // GET: /Admin/Category/
        public ActionResult Index()
        {
            localhost.Service service = new localhost.Service();
            var model = service.GetCategory().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Insert()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Insert(localhost.Category cate)
        {
            String Id = null;
            try
            {
                localhost.Service service = new localhost.Service();
                Id = service.InsertCategory(cate);
            }
            catch (Exception ex)
            {

                
            }

            if (!String.IsNullOrEmpty(Id))
            {
                TempData["AlertMessage"] = "Inserted Successfully";

                return View("Insert");
            }
            else
            {
                TempData["AlertMessage"] = "Failed";

                return View("Insert");
            }
        }


        [HttpGet]
        public ActionResult Edit(String Id)
        {
            localhost.Service service = new localhost.Service();
            var model = service.GetCategoryById(Id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(localhost.Category cate)
        {
            String Id = null;
            try
            {
                localhost.Service service = new localhost.Service();
                Id = service.UpdateCategory(cate);
            }
            catch (Exception ex)
            {

                TempData["AlertMessage"] = ex.ToString();

            }

            if (!String.IsNullOrEmpty(Id))
            {
                TempData["AlertMessage"] = "Edited Successfully";

                return RedirectToAction("Index");
            }
            else
            {
                TempData["AlertMessage"] = "Failed";

                return RedirectToAction("Index");
            }
        }

        [HttpDelete]
        public ActionResult Delete(String Id)
        {
            localhost.Service service = new localhost.Service();
            service.DeleteCategory(Id);
            return RedirectToAction("Index");
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebStore.Areas.Admin.Controllers
{
    public class CustomerController : BaseController
    {
        //
        // GET: /Admin/Customer/
        public ActionResult Index()
        {
            localhost.Service service = new localhost.Service();
            var model = service.GetCustomers().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Insert()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Insert(localhost.Customer cus)
        {

            String Id = null;
            try
            {
                localhost.Service service = new localhost.Service();
                if (!service.isCustomerExist(cus.Username))
                {

                    var confirm = HttpContext.Request.Params.Get("confirm");
                    try
                    {

                        if (cus.Password.Equals(confirm))
                        {
                            Id = service.InsertCustomer(cus);
                            if (Id != null)
                            {
                                TempData["AlertMessage"] = "Inserted Successfully";
                                return RedirectToAction("Index", "Customer");
                            }
                            else
                            {
                                TempData["AlertMessage"] = "Failed";
                                ModelState.AddModelError("", "Thêm người dùng không thành công");
                            }
                        }
                    }
                    catch (Exception)
                    {

                        ModelState.AddModelError("", "Mật khẩu không khớp");
                    }
                }

            }
            catch (Exception)
            {


            }

            TempData["AlertMessage"] = "User already existed!";

            return View("Insert");

        }


    }
}
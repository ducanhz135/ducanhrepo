﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStore.localhost;

namespace WebStore.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        //
        // GET: /Admin/Product/
        public ActionResult Index(float? firstPrice, float? secondPrice)
        {
            localhost.Service service = new localhost.Service();
            if (firstPrice != null && secondPrice != null)
            {
                ViewBag.First = firstPrice;
                ViewBag.Second = secondPrice;
                var model = service.GetProductsByPrice(firstPrice, secondPrice).ToList();
                return View(model);
            }
            else
            {
                var model = service.GetProducts().ToList();
                return View(model);
            }

        }

        public void GetCategories()
        {
            localhost.Service service = new localhost.Service();
            List<Category> catList = service.GetCategory().ToList();
            List<SelectListItem> li = new List<SelectListItem>();

            for (int i = 0; i < catList.Count; i++)
            {
                li.Add(new SelectListItem { Text = catList.ElementAt(i).Name, Value = catList.ElementAt(i).Id });
            }
            ViewData["categories"] = li;
            System.Diagnostics.Debug.WriteLine("Number of Catgory: " + li.Count);
        }
        [HttpGet]
        public ActionResult Insert()
        {
            GetCategories();
            return View();
        }
        [HttpPost]
        public ActionResult Insert(localhost.Product pro)
        {
            String Id = null;
            try
            {
                localhost.Service service = new localhost.Service();
                Id = service.InsertProduct(pro);
            }
            catch (Exception)
            {

                throw;
            }

            GetCategories();
            if (!String.IsNullOrEmpty(Id))
            {
                TempData["AlertMessage"] = "Inserted Successfully";

                return View("Insert");
            }
            else
            {
                TempData["AlertMessage"] = "Failed";

                return View("Insert");
            }
        }


        [HttpGet]
        public ActionResult Edit(String Id)
        {
            GetCategories();
            localhost.Service service = new localhost.Service();
            var model = service.GetProductById(Id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(localhost.Product prod)
        {
            String Id = null;
            try
            {
                localhost.Service service = new localhost.Service();
                Id = service.UpdateProduct(prod);
            }
            catch (Exception ex)
            {

                TempData["AlertMessage"] = ex.ToString();
            }

            if (!String.IsNullOrEmpty(Id))
            {
                TempData["AlertMessage"] = "Edited Successfully";

                return RedirectToAction("Index");
            }
            else
            {
                TempData["AlertMessage"] = "Failed";

                return RedirectToAction("Index");
            }
        }

        [HttpDelete]
        public ActionResult Delete(String Id)
        {
            localhost.Service service = new localhost.Service();
            Boolean result = service.DeleteProduct(Id);
            return RedirectToAction("Index");


        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebStore.Areas.Admin.Models
{
    public class LoginModel
    {
        [Display(Name = "Login_UserName")]
        [Required(ErrorMessage="please input username")]
        public string UserName { get; set; }

        [Display(Name = "Login_Password")]
        [Required(ErrorMessage = "please input password")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
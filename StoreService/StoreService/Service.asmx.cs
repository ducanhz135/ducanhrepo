﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace StoreService
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        private ShopDBDataContext db = null;
        public Service()
        {
            db = new ShopDBDataContext();
        }
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public String InsertCustomer(Customer cus)
        {

            db.Customers.InsertOnSubmit(cus);
            db.SubmitChanges();
            return cus.Id;
        }
        [WebMethod]
        public List<Customer> GetCustomers()
        {

            return db.Customers.ToList();
        }
        [WebMethod]
        public bool isCustomerExist(String Username)
        {
            var user = db.Customers.SingleOrDefault(m => m.Username == Username);
            if (user != null)
            {
                return true;
            }
            return false;
        }
        [WebMethod]
        public Customer getCustomerByName(string Username)
        {
            return db.Customers.SingleOrDefault(m => m.Username == Username);
        }
        //[WebMethod]
        //public Customer getCustomerByName(string Username, string Password)
        //{
        //    return db.Customers.SingleOrDefault(m => m.Username == Username && m.Password == Password);
        //}
        [WebMethod]
        public int LoginCustomer(string Username, string Password)
        {
            var result = db.Customers.SingleOrDefault(m => m.Username == Username);
            if (result == null)
            {
                return 0;
            }
            else
            {


                if (result.Password == Password)
                    {
                        return 1;
                    }
                    return -1;
                
            }
        }
        [WebMethod]
        public int LoginAdmin(string Username, string Password)
        {
            var result = db.Admins.SingleOrDefault(m => m.username == Username);
            if (result == null)
            {
                return 0;
            }
            else
            {


                if (result.password == Password)
                {
                    return 1;
                }
                return -1;

            }
        }

        [WebMethod]
        public Admin getAdminByName(string Username)
        {
            return db.Admins.SingleOrDefault(m => m.username == Username);
        }
        [WebMethod]
        public Customer GetCustomerById(String Id)
        {

            return db.Customers.SingleOrDefault(m => m.Id == Id);
        }

        [WebMethod]
        public String InsertCategory(Category cate)
        {

            db.Categories.InsertOnSubmit(cate);
            db.SubmitChanges();
            return cate.Id;
        }
        [WebMethod]
        public List<Category> GetCategory()
        {

            return db.Categories.ToList();
        }

        [WebMethod]
        public String UpdateCategory(Category cate)
        {
            Category item = db.Categories.SingleOrDefault(i => i.Id == cate.Id);
            item.Name = cate.Name;

            db.SubmitChanges();
            return item.Id;
        }

        [WebMethod]
        public bool DeleteCategory(String Id)
        {
            try
            {
                Category item = db.Categories.SingleOrDefault(i => i.Id == Id);
                db.Categories.DeleteOnSubmit(item);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        [WebMethod]
        public Category GetCategoryById(String Id)
        {

            return db.Categories.SingleOrDefault(m => m.Id == Id);
        }


        [WebMethod]

        public String InsertProduct(Product prod)
        {

            db.Products.InsertOnSubmit(prod);
            db.SubmitChanges();

            return prod.Id;
        }

        [WebMethod]

        public String UpdateProduct(Product prod)
        {

            Product item = db.Products.SingleOrDefault(i => i.Id == prod.Id);
            item.Name = prod.Name;
            item.Price = prod.Price;
            item.Detail = prod.Detail;
            item.CateId = prod.CateId;
            db.SubmitChanges();
            return item.Id;
        }
        [WebMethod]
        public bool DeleteProduct(String Id)
        {
            try
            {
                Product item = db.Products.SingleOrDefault(i => i.Id == Id);
                db.Products.DeleteOnSubmit(item);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        [WebMethod]
        public List<Product> GetProducts()
        {

            return db.Products.ToList();
        }

        [WebMethod]
        public List<string> ListName(string keyword)
        {
            return db.Products.Where(x => x.Name.Contains(keyword)).Select(x => x.Name).ToList();
        }
        [WebMethod]
        public List<Product> searchProduct(string keyword)
        {
            return db.Products.Where(x => x.Name.Contains(keyword)).ToList();
        }
        [WebMethod]
        public List<Product> GetProductsByPrice(float? firstPrice, float? secondPrice)
        {

            if (firstPrice != null && secondPrice != null)
            {
                return db.Products.Where(m => m.Price >= firstPrice && m.Price <= secondPrice).ToList();
            }
            else
            {
                return db.Products.ToList();
            }
        }
        [WebMethod]
        public Product GetProductById(String Id)
        {

            return db.Products.SingleOrDefault(m => m.Id == Id);
        }

        [WebMethod]
        public String InsertOrder(TheOrder ord)
        {

            db.TheOrders.InsertOnSubmit(ord);
            db.SubmitChanges();
            return ord.Id;
        }
        [WebMethod]
        public List<TheOrder> GetOrders()
        {

            return db.TheOrders.ToList();
        }
        [WebMethod]
        public String UpdateOrders(TheOrder ord)
        {
            TheOrder item = db.TheOrders.SingleOrDefault(i => i.Id == ord.Id);
            item.Name = ord.Name;
            item.CusId = ord.CusId;
            item.ShipAddress = ord.ShipAddress;
            db.SubmitChanges();
            return item.Id;
        }

        [WebMethod]
        public bool DeleteOrder(String Id)
        {
            try
            {
                TheOrder item = db.TheOrders.SingleOrDefault(i => i.Id == Id);
                db.TheOrders.DeleteOnSubmit(item);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        [WebMethod]
        public TheOrder GetOrderById(String Id)
        {

            return db.TheOrders.SingleOrDefault(m => m.Id == Id);
        }


        [WebMethod]
        public String InsertOrderDetail(OrderDetail ord)
        {

            db.OrderDetails.InsertOnSubmit(ord);
            db.SubmitChanges();
            return ord.OrdId+ord.ProdId;
        }
        //[WebMethod]
        //public List<OrderDetail> GetOrderDetail()
        //{

        //    return db.OrderDetails.ToList();
        //}
        //[WebMethod]
        //public String UpdateOrderDetail(OrderDetail ord)
        //{
        //    OrderDetail item = db.OrderDetails.SingleOrDefault(i => i.Id == ord.Id);
        //    item.Quantity = ord.Quantity;
        //    item.Price = ord.Price;
            
        //    item.OrdId = ord.OrdId;

        //    db.SubmitChanges();
        //    return item.Id;
        //}

        //[WebMethod]
        //public bool DeleteOrderDetail(String Id)
        //{
        //    try
        //    {
        //        OrderDetail item = db.OrderDetails.SingleOrDefault(i => i.Id == Id);
        //        db.OrderDetails.DeleteOnSubmit(item);
        //        db.SubmitChanges();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //}
        //[WebMethod]
        //public OrderDetail GetOrderDetailById(String Id)
        //{

        //    return db.OrderDetails.SingleOrDefault(m => m.Id == Id);
        //}

    }
}
